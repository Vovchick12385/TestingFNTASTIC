#include <iostream>
#include <string>
#include <unordered_map>

void ConvertString(std::string& str)
{
	std::unordered_map<char, int> CheckMap;
	for (auto symbol : str)
	{
		CheckMap[tolower(symbol)]++;	
	}
	for (auto it = str.begin(); it!= str.end();++it)
	{
		if (CheckMap.at(*it) > 1)
		{
			*it = ')';
			continue;
		}
		*it = '(';
	}
}

int main()
{
	std::string str;
	std::getline(std::cin, str);
	ConvertString(str);
	std::cout << str << std::endl;
	return 0;
}